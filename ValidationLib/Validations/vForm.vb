﻿Imports System.Windows.Forms

Public Class vForm

    Private form As Hashtable
    Private values As Hashtable

    Sub New()
        form = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum FormRules
        required
        requiredvalue
        maxlength
        rangelenght
    End Enum

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In form

            'required
            If CType(pair.Value, FormRules).Equals(FormRules.required) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Trim = "" Then
                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = System.Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é obrigatório!", c.getNome))
                    erros += 1
                End If

                'numericUpDown
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is NumericUpDown AndAlso cConversions.NumericUpDown(cConversions.rcField(pair.Key).objeto).Value = 0 Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As NumericUpDown = cConversions.NumericUpDown(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é obrigatório!", c.getNome))
                    erros += 1
                End If

                'combobox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is ComboBox AndAlso cConversions.Combobox(cConversions.rcField(pair.Key).objeto).SelectedValue = 0 Then
                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As NumericUpDown = cConversions.NumericUpDown(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é obrigatório!", c.getNome))
                    erros += 1
                End If

                'required value
            ElseIf CType(pair.Value, FormRules).Equals(FormRules.requiredvalue) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Trim <> values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é deve ser igual a {1}!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

                'numericUpDown
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.NumericUpDown(cConversions.rcField(pair.Key).objeto).Value = values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As NumericUpDown = cConversions.NumericUpDown(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é deve ser igual a {1}!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

            ElseIf CType(pair.Value, FormRules).Equals(FormRules.maxlength) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Length <= values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é deve ter até {1} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

                'numericUpDown
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.NumericUpDown(cConversions.rcField(pair.Key).objeto).Value.ToString.Length <= values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As NumericUpDown = cConversions.NumericUpDown(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} é deve ter até {1} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

            ElseIf CType(pair.Value, FormRules).Equals(FormRules.rangelenght) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Length >= values(pair.Key)(0) And cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Length <= values(pair.Key)(1) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} deve estar entre {1} e {2} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

                'numericUpDown
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso cConversions.NumericUpDown(cConversions.rcField(pair.Key).objeto).Value.ToString.Length >= values(pair.Key) And cConversions.NumericUpDown(cConversions.rcField(pair.Key).objeto).Value.ToString.Length <= values(pair.Key) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As NumericUpDown = cConversions.NumericUpDown(c.objeto)
                    t.ForeColor = Drawing.Color.Red
                    'AddHandler t.LostFocus, AddressOf MudaCor

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} deve estar entre {1} e {2} caracteres!", c.getNome, values(pair.Key)))
                    erros += 1
                End If

            End If

        Next

        Return Not erros > 0

    End Function

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal stringValue As String)
        form(c) = regra
        values(c) = stringValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal intValue As Integer)
        form(c) = regra
        values(c) = intValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules, ByVal intValue() As Integer)
        form(c) = regra
        values(c) = intValue
    End Sub

    Sub addRule(ByVal c As cField, ByVal regra As vForm.FormRules)
        form(c) = regra
    End Sub

End Class
