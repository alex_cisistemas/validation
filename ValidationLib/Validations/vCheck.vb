﻿Imports System.Windows.Forms

Public Class vCheck

    Private Shared mensagens As New List(Of String)

    Private check As Hashtable
    Private values As Hashtable

    Sub New()
        check = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum CheckRules
        atLeastOne
    End Enum

    Sub addRule(ByVal c As cField(), ByVal regra As vCheck.CheckRules, Optional ByVal nomeGrupo As String = "")
        check(c) = regra
        values(c) = nomeGrupo
    End Sub

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In check

            Dim result As Boolean = False

            'atleastone
            If CType(pair.Value, CheckRules).Equals(CheckRules.atLeastOne) Then

                'If TypeOf pair.Key Is cField() Then

                result = False

                For Each c As cField In CType(pair.Key, cField())

                    If Not IsNothing(c) Then
                        If (TypeOf c.objeto Is RadioButton) Then
                            result = result Or cConversions.RadioButton(c.objeto).Checked
                        ElseIf (TypeOf c.objeto Is CheckBox) Then
                            result = result Or cConversions.CheckBox(c.objeto).Checked
                        End If
                    Else
                        Exit For
                    End If

                Next

                If Not result Then
                    mensagens.Add(String.Format("O grupo {0} precisa de pelo menos uma opção selecionada!", values(pair.Key)))
                    erros += 1
                End If

            End If


        Next

        Return Not erros > 0

    End Function

End Class
