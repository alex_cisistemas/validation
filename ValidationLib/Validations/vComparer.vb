﻿Public Class vComparer

    Private comparer As Hashtable
    Private values As Hashtable
    Private mensagens As New List(Of String)

    Sub New()
        comparer = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum ComparerRules
        greaterThan
        lessThan
        equals
        different
        greaterEquals
        lessEquals
        between
    End Enum

    Sub addRule(ByVal c As cField(), ByVal regra As ComparerRules)
        comparer(c) = regra
    End Sub


End Class
