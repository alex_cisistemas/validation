﻿Imports System.Windows.Forms

Public Class vUser

    Private user As Hashtable
    Private values As Hashtable

    Sub New()
        user = New Hashtable
        values = New Hashtable
    End Sub

    Public Enum UserRules
        cnpj
        cpf
        email
        url
    End Enum

    Private mensagens As New List(Of String)

    Sub addRule(ByVal c As cField, ByVal regra As UserRules)
        user(c) = regra
    End Sub

    Function validate() As Boolean

        Dim erros As Integer = 0

        For Each pair As DictionaryEntry In user

            'email
            If CType(pair.Value, UserRules).Equals(UserRules.email) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso Not cFunctions.ValidaEmail(cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Trim) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} não é um email válido!", c.getNome))
                    erros += 1

                End If

                'CPF
            ElseIf CType(pair.Value, UserRules).Equals(UserRules.cpf) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso Not cFunctions.ValidaCPF(cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Trim) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} não é um CPF válido!", c.getNome))
                    erros += 1

                End If

                'CNPJ
            ElseIf CType(pair.Value, UserRules).Equals(UserRules.cnpj) Then

                'textbox
                If TypeOf pair.Key Is cField And TypeOf cConversions.rcField(pair.Key).objeto Is TextBox AndAlso Not cFunctions.ValidaCNPJ(cConversions.Textbox(cConversions.rcField(pair.Key).objeto).Text.Trim) Then

                    Dim c As cField = cConversions.rcField(pair.Key)
                    Dim t As TextBox = cConversions.Textbox(c.objeto)
                    t.ForeColor = Drawing.Color.Red

                    c.showMessage = True
                    mensagens.Add(String.Format("O cField {0} não é um CNPJ válido!", c.getNome))
                    erros += 1

                End If

            End If

        Next

        Return Not erros > 0

    End Function

End Class
