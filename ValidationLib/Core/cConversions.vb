﻿Imports System.Windows.Forms

Public Class cConversions


    Shared Function Textbox(ByVal obj As Object) As TextBox
        If TypeOf obj Is TextBox Then
            Return CType(obj, TextBox)
        Else
            Return New TextBox
        End If
    End Function

    Shared Function NumericUpDown(ByVal obj As Object) As NumericUpDown
        If TypeOf obj Is NumericUpDown Then
            Return CType(obj, NumericUpDown)
        Else
            Return New NumericUpDown
        End If
    End Function

    Shared Function Combobox(ByVal obj As Object) As ComboBox
        If TypeOf obj Is ComboBox Then
            Return CType(obj, ComboBox)
        Else
            Return New ComboBox
        End If
    End Function

    Shared Function CheckBox(ByVal obj As Object) As CheckBox
        If TypeOf obj Is CheckBox Then
            Return CType(obj, CheckBox)
        Else
            Return New CheckBox
        End If
    End Function

    Shared Function RadioButton(ByVal obj As Object) As RadioButton
        If TypeOf obj Is RadioButton Then
            Return CType(obj, RadioButton)
        Else
            Return New RadioButton
        End If
    End Function


    Shared Function rcField(ByVal obj As Object) As cField
        If TypeOf obj Is cField Then
            Return CType(obj, cField)
        Else
            Return New cField
        End If
    End Function

End Class
